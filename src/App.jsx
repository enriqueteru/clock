import React from "react";
import { DigitalClock, CountDown, StopWatch } from "./components/DigitalClock";

function App() {
  return (
    <div className="row">
      <div className="col">
        <DigitalClock />
      </div>
      <div className="col" style={{backgroundColor:'whitesmoke'}}>
      <CountDown />
      </div>
      <div className="col">
      <StopWatch />
      </div>
    </div>
  );
}

export default App;
