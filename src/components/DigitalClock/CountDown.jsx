import React, { useEffect, useState } from "react";

const CountDown = () => {
  const [counter, setCounter] = React.useState(60);

  // Third Attempts
  React.useEffect(() => {
    const timer = counter > 0 && setInterval(() => setCounter(counter - 1), 1000);
    return () => clearInterval(timer);
  }, [counter]);

  if(counter <= 0){
    setCounter(60)
  }

  return (
    <div className="App">
      <span>COUNTDOWN</span>
      <h2>{counter}</h2>
    </div>
  );
};

export default CountDown;
