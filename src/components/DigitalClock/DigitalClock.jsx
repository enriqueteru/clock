import { useEffect, useState } from "react";

const DigitalClock = () => {
  const [clockState, setClockState] = useState();

  useEffect(() => {
    setInterval(() => {
      const date = new Date();
      setClockState(date.toLocaleTimeString());
    }, 1000);
  }, []);

  return (
    <div>
       <span>TIME</span>
      <h2 style={{ textAlign: "center", width: '100%' }}>{clockState}</h2>
    </div>
  );
};

export default DigitalClock;
